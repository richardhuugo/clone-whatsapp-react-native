import {
    MODIFICAR_EMAIL_LOGIN,
    MODIFICAR_SENHA_LOGIN,
    MODIFICA_EMAIL,
    MODIFICA_SENHA,
    MODIFICA_NOME,
    SUCESSO_LOGIN,
    ERRO_LOGIN,
    SUCESSO,
    CADASTRO_USUARIO_ERRO,
    LOAD_LOGIN_ANDAMENTO,
    LOAD_CADASTRO_ANDAMENTO,
    LOGOUT
} from '../Actions/types';
const INITIAL_STATE = {
    nome: '',
    email: '',
    senha: '',
    erroCadastro: '',
    emailLogin:'',
    senhaLogin:'',
    erroLogin:'',
    loading_login: false,
    loading_cadastro:false
   
    
}

export default (state =INITIAL_STATE , action) => {
 
    switch(action.type){
    
        case SUCESSO_LOGIN:
            return { ...state, erroLogin:''}
        case ERRO_LOGIN:
            return { ...state, erroLogin: action.payload}
        case MODIFICAR_EMAIL_LOGIN:
            return { ...state, emailLogin: action.payload}  
        case MODIFICAR_SENHA_LOGIN:
            return { ...state, senhaLogin: action.payload}
        case MODIFICA_EMAIL:
            return { ...state, email: action.payload} 
        case MODIFICA_SENHA:
            return { ...state, senha: action.payload}
        case MODIFICA_NOME:
            return { ...state, nome: action.payload}
        case SUCESSO:
            return { ...state, emailLogin: action.email, nome:'', email:'', senha:''}
        case LOGOUT:
          return { ...state, emailLogin: '',senhaLogin:'' }
        case CADASTRO_USUARIO_ERRO:
            return { ...state, erroCadastro: action.payload}
        case LOAD_LOGIN_ANDAMENTO:
            return { ...state, loading_login: action.payload}
        case LOAD_CADASTRO_ANDAMENTO:
            return { ...state,loading_cadastro:action.payload }
        default:
            return state;
    }

    
   
 
    
}