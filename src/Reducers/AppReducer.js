const INITIAL_STATE = {
    adiciona_contato_email: '',
    mensagemContato: '',
    cadastro_resultado_inclusao: false,
    mensagem: ''
};
import {MODIFICA_ADICIONA_CONTATO,    
        ADICIONA_CONTATO_FALHA,
        ADICIONA_CONTATO_SUCESS,
        MODIFICA_MENSAGEM,MENSAGEM_ENVIADA
        } from '../Actions/types'

export default (state = INITIAL_STATE,  action ) => {
    switch(action.type){
        case MODIFICA_ADICIONA_CONTATO:
        return { ...state, adiciona_contato_email:action.payload}      
        case ADICIONA_CONTATO_FALHA:
            return {...state, mensagemContato:action.payload}
        case ADICIONA_CONTATO_SUCESS:
            return {...state, cadastro_resultado_inclusao: action.payload,adiciona_contato_email:''}
        case MODIFICA_MENSAGEM:
            return{ ...state, mensagem:action.payload}
        case MENSAGEM_ENVIADA:
            return {...state, mensagem:''}

        default:
            return state;
    }
};