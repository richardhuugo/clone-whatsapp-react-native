import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import AppReducer from './AppReducer';
import ListarContatosReducer from './ListarContatosReducer';
import ListarConversaReducer from './ListarConversaReducer';
import ListarConversasReducer from './ListarConversasReducer';

export default combineReducers({
   
   AuthReducer,
   AppReducer,
   ListarContatosReducer,
   ListarConversaReducer,
   ListarConversasReducer
   
});