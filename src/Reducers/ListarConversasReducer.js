import {LISTAR_USUARIOS_ALL_CONVERSAS}  from '../Actions/types';
const INITIAL_STATE = {}

export default (state= INITIAL_STATE, action) =>{

    switch(action.type){
        case LISTAR_USUARIOS_ALL_CONVERSAS:
            return action.payload;  
            
        default:
            return state;
    }
}