import React from 'react';
import {StyleSheet} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';

import formLogin from './Components/formLogin';
import formCadastro from './Components/formCadastro';
import BoasVindas from './Components/BoasVindas';
import Home from './Components/Home';
import AdicionarContatos from './Components/AdicionarContatos';
import Chat from './Components/Chat';
import firebase from 'firebase';


export default class Routes extends React.Component{

 
  render(){
    return(
      <Router  navigationBarStyle={styles.all} titleStyle={styles.textAll} >
      <Scene
          key="root"
          component={formLogin}            
        >
      <Scene key='formLogin' component={formLogin} title="Login" hideNavBar={true} />       
      <Scene    key='Home' component={Home} title="Home" hideNavBar={true} />
      <Scene key='formCadastro' component={formCadastro} title="Cadastro" hideNavBar={false}/>
      <Scene key='boasVindas' component={BoasVindas} navigationBarStyle={styles.navBar} title="Bem vindo" hideNavBar={true} /> 
      <Scene key='adicionarContato'    component={AdicionarContatos} title="Adicionar Contato" /> 
      <Scene key='Chat'    component={Chat} title="Chat"  hideNavBar={false} />  
        
        </Scene>
    </Router>
    );
  }


}



const styles = StyleSheet.create({
    all:{
      backgroundColor:'#115e54'
    },
    textAll:{
      color:'#fff'
    },
    navBar: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'red', // changing navbar color
    }
  })