export const MODIFICAR_EMAIL_LOGIN = 'modificar_email_login';
export const MODIFICAR_SENHA_LOGIN ='modificar_senha_login';
export const MODIFICA_EMAIL ='modifica_email';
export const MODIFICA_SENHA ='modifica_senha';
export const MODIFICA_NOME ='modifica_nome';
export const SUCESSO_LOGIN ='sucessoLogin';
export const ERRO_LOGIN ='erroLogin';
export const SUCESSO ='sucesso';
export const CADASTRO_USUARIO_ERRO ='cadastro_usuario_erro';
export const LOAD_LOGIN_ANDAMENTO = 'load_login_andamento';
export const LOAD_CADASTRO_ANDAMENTO = "load_cadastro_andamento";
export const MODIFICA_ADICIONA_CONTATO = "modifica_adiciona_contato";
export const ADICIONA_CONTATO_SUCESS =  "adiciona_contato_sucess";
export const ADICIONA_CONTATO_FALHA =  "adiciona_contato_falha";
export const MENSAGEM_CONTATO_FALHA = "E-mail informado não corresponde ao usuário válido.";
export const LISTAR_CONTATO_USUARIO = "listar_contato_usuario"
export const MODIFICA_MENSAGEM = "modifica_mensagem";
export const LISTA_CONVERSA_USUARIO = "lista_conversa_usuario";
export const MENSAGEM_ENVIADA = 'mensagem_enviada';
export const LISTAR_USUARIOS_ALL_CONVERSAS = 'listar_usuarios_all_conversas';
export const LOGOUT = "logout";


