import firebase from 'firebase';
import {Actions} from 'react-native-router-flux';
import b64 from 'base-64';
import {
    MODIFICAR_EMAIL_LOGIN,
    MODIFICAR_SENHA_LOGIN,
    MODIFICA_EMAIL,
    MODIFICA_SENHA,
    MODIFICA_NOME,
    SUCESSO_LOGIN,
    ERRO_LOGIN,
    SUCESSO,
    CADASTRO_USUARIO_ERRO,
    LOAD_LOGIN_ANDAMENTO,
    LOAD_CADASTRO_ANDAMENTO
} from './types';

export const modificarEmailLogin = (texto) =>{
    return {
        type:MODIFICAR_EMAIL_LOGIN,
        payload:texto
    }
}
export const modificarSenhaLogin = (texto) => {
    return {
        type:MODIFICAR_SENHA_LOGIN,
        payload:texto
    }
}

export const modificarEmail = (texto) => {
    return {
        type:MODIFICA_EMAIL,
        payload: texto
    }
}

export const modificarSenha = (senha) => {
    return {
        type:MODIFICA_SENHA,
        payload: senha
    }
}

export const modificarNome = (nome) => {
    return {
        type:MODIFICA_NOME,
        payload: nome
    }
}

export const loginUsuario = ({ emailLogin, senhaLogin}) =>{
    return dispatch => {
        dispatch({type:LOAD_LOGIN_ANDAMENTO, payload:true});
        const usr = firebase.auth();
        usr.signInWithEmailAndPassword(emailLogin,senhaLogin).then(sucesso => loginSucess(dispatch)).catch(
        (erro) =>{ loginError(erro, dispatch)} );
               
           
       
    }
  
    
}

const loginSucess = ( dispatch) =>{
    dispatch({ type:SUCESSO_LOGIN });
    dispatch({type:LOAD_LOGIN_ANDAMENTO, payload:false});
    Actions.Home();
    
}
const loginError = (error, dispatch) => {
    dispatch({type:LOAD_LOGIN_ANDAMENTO, payload:false});
    dispatch({ type:ERRO_LOGIN, payload:error.message });
    
}


export const cadastrarUsuario = ({nome, email, senha}) => {    
   
    return dispatch  => {
        dispatch({type:LOAD_CADASTRO_ANDAMENTO, payload:true});
        firebase.auth().createUserWithEmailAndPassword(email, senha)
        .then( va => {
            let emailB64 = b64.encode(email);
            firebase.database()
                .ref(`/contatos/${emailB64}`)
                .push({ nome })
                .then(value => cadastroUsuarioSucesso(email,dispatch) );

            
        })
        .catch( (erro) => { cadastroUsuarioErro(erro, dispatch);}  );   
    }        
}

const cadastroUsuarioSucesso = (email,dispatch) =>{
    dispatch({ type:SUCESSO, email:email });
    dispatch({type:LOAD_CADASTRO_ANDAMENTO, payload:false});
     Actions.boasVindas();
   
}

const cadastroUsuarioErro = (erro, dispatch) => {
    dispatch({type:LOAD_CADASTRO_ANDAMENTO, payload:false});
    dispatch({type:CADASTRO_USUARIO_ERRO,payload:erro.message});  
}