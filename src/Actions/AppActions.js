import { MODIFICA_ADICIONA_CONTATO,
        ADICIONA_CONTATO_FALHA,
        ADICIONA_CONTATO_SUCESS,
        MENSAGEM_CONTATO_FALHA,
        LISTAR_CONTATO_USUARIO,
        MODIFICA_MENSAGEM,
        LISTA_CONVERSA_USUARIO ,
        MENSAGEM_ENVIADA,
        LOGOUT,
        LISTAR_USUARIOS_ALL_CONVERSAS} from './types';
import b64 from 'base-64';
import firebase from 'firebase';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';
export const modificaAdicionaContatoEmail = (email) => {
    
    return {
        type:MODIFICA_ADICIONA_CONTATO,
        payload:email
    }
}

export const adicionaContato = (email) => {
    
    return dispatch => {
   
       
        let emailB64 = b64.encode(email);
        firebase.database().ref(`/contatos/${emailB64}`)
        .once('value')
        .then(snapshot =>{
            if(snapshot.val()) {

                const dadosUsuario = _.values(snapshot.val());


                const { currentUser } = firebase.auth();
                let emailUsuarioB64 = b64.encode(currentUser.email);

               // firebase.database
                firebase.database().ref(`/usuarios_contatos/${emailUsuarioB64}`)
                .push({email, nome:dadosUsuario[0].nome})
                .then(sucesso =>contatoAdSucesso(dispatch))
                .catch( erro => contatoAdErro(erro.message, dispatch) );
             
            } else {
                dispatch({  type:ADICIONA_CONTATO_FALHA,payload:MENSAGEM_CONTATO_FALHA});              
            }               
        })     
           
       
    }
    
}

const contatoAdSucesso = (dispatch) =>{

    
    dispatch({  type:ADICIONA_CONTATO_SUCESS, payload:true});
}

export const  removeViewAdicionarContato = () => {
    return  {type:ADICIONA_CONTATO_SUCESS, payload:false}    
}
const contatoAdErro = (erro, dispatch) =>{
    dispatch({  type:ADICIONA_CONTATO_FALHA,payload:erro}); 
}




export const contatosUsuarioFetch = () =>{
    const { currentUser } =   firebase.auth();

    return (dispatch) =>{
        let emailUsuarioB64 = b64.encode( currentUser.email);

        firebase.database().ref(`/usuarios_contatos/${emailUsuarioB64}`)
        .on('value', snapshot => {
            dispatch({ type: LISTAR_CONTATO_USUARIO, payload:snapshot.val()})
        })
    }
}

export const modificaMensagem = texto =>{
    return({
        type:MODIFICA_MENSAGEM,
        payload:texto
    });
}

export const enviarMensagem = (mensagem, contatoNome,contatoEmail) =>{
    const {currentUser} = firebase.auth();
    const usuarioEmail = currentUser.email;

    return dispatch =>{
        //dados contato
        dispatch({ type:MENSAGEM_ENVIADA });
        //dados usuario
        const usuarioEmailB64 = b64.encode(usuarioEmail);
        const contatoEmailB64 = b64.encode(contatoEmail);
        firebase.database()
        .ref(`/mensagens/${usuarioEmailB64}/${contatoEmailB64}`)
        .push({mensagem,tipo:'e'})       
        .then(() => {
            firebase.database().ref(`/mensagens/${contatoEmailB64}/${usuarioEmailB64}`)
            .push({mensagem, tipo:'r'}).then(() => { dispatch({ type:MENSAGEM_ENVIADA }) })
        })
        .then(() =>{
            //armazenamento cabecalhos usuario
            firebase.database().ref(`/usuario_conversas/${usuarioEmailB64}/${contatoEmailB64}`)
            .set({nome:contatoNome, email:contatoEmail})
        })
        .then(() =>{
            firebase.database().ref(`/contatos/${usuarioEmailB64}`)
            .once("value")
            .then(snapshot =>{
                 // armazenar cabecalho do contato               
               const dadosUsuario = _.values(snapshot.val());
                firebase.database().ref(`/usuario_conversas/${contatoEmailB64}/${usuarioEmailB64}`)
                .set({nome:dadosUsuario[0].nome, email:usuarioEmail})
            })
        })
       
       
    }
}


export const conversaUsuarioFecth = (contatoEmail) =>{
    const {currentUser } = firebase.auth();
    // emails na base 64
    let usuarioEmailB64 = b64.encode(currentUser.email);
    let contatoEmailB64 = b64.encode(contatoEmail);
    
    return dispatch => {

        firebase.database().ref(`/mensagens/${usuarioEmailB64}/${contatoEmailB64}`)
        .on("value", snapshot => {
            dispatch({type:LISTA_CONVERSA_USUARIO, payload: snapshot.val()})
        })
    }
}

export const conversasUsuariosFecth = () => {
    const {currentUser} = firebase.auth();

    let usuarioEmailB64 = b64.encode(currentUser.email);

    return dispatch => {
        firebase.database().ref(`/usuario_conversas/${usuarioEmailB64}`)
        .on("value", snapshot =>{
            dispatch({ type:LISTAR_USUARIOS_ALL_CONVERSAS, payload: snapshot.val() });
        })
    }

}


export const efetuarLogout = () =>{
    return dispatch => {
        firebase.auth().signOut().then( sucesso =>{
            Actions.formLogin()   
            dispatch({ type:LOGOUT});            
        })
    }
}