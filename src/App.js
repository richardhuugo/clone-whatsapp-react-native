import React,{Component} from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import Routes from './Routes';
import reducers from './Reducers';
import ReduxThunk from 'redux-thunk';

export default  class App extends Component {



    render(){
        return(
            <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
            <Routes />
        </Provider>
        );
    }

}
