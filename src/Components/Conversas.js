import React from 'react';
import {View, Text,StyleSheet,ListView,TouchableHighlight} from 'react-native';
import {connect} from 'react-redux';
import {conversasUsuariosFecth } from '../Actions/AppActions/';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';

class Conversas extends React.Component{
  componentWillMount(){
    this.props.conversasUsuariosFecth();
    this.atualizaListaConversas(this.props.conversas);
  }
  componentWillReceiveProps(nextProps){
    if(this.props.conversas != nextProps.conversas){
      this.atualizaListaConversas(nextProps.conversas);
    }
    this.atualizaListaConversas(nextProps.conversas);
  }


  atualizaListaConversas( conversas ){
    const ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 != r2});
    this.fontAtualizaConversas = ds.cloneWithRows(conversas);
  }
  renderRowLine(conversa){
    return (
      <TouchableHighlight
          onPress={() => Actions.Chat({
            title: conversa.nome,
            contatoNome: conversa.nome,
            contatoEmail: conversa.email
          }) }
      >
      <View style={{flex:1, borderBottomWidth:1, borderColor:'#CCC', padding:20}}>      
        <Text>{conversa.nome}</Text>        
      </View>
      </TouchableHighlight>
    );
  }
  render(){
    return(
      <View style={[ styles.container, { backgroundColor: '#fff' } ]}>
        <ListView 
        enableEmptySections
        dataSource={this.fontAtualizaConversas}
        renderRow={this.renderRowLine}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });
mapStateToProps = state =>{  
  const conversas = _.map(state.ListarConversasReducer, (val, uid) =>{
    return { ...val, uid}
  })
  return {
    conversas
  }
}
export default connect(mapStateToProps, {conversasUsuariosFecth})(Conversas);