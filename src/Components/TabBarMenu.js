import React from 'react';
import {Actions} from 'react-native-router-flux';
import {View, Text,StatusBar,Image,TouchableHighlight} from 'react-native';
import {TabBar} from 'react-native-tab-view';
import {connect} from 'react-redux';
import {  removeViewAdicionarContato, efetuarLogout } from '../Actions/AppActions';


const TabBarMenu =  props => (
   
        <View style={{backgroundColor:'#115354', elevation:0, marginBottom:6}}>
        <StatusBar backgroundColor="#115354"/>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
        <View style={{height:50, justifyContent:'center'}}>
            <Text style={{ color:'#fff', fontSize:20, marginLeft:20}}> WhatsApp Clone</Text>
        </View>

            <View style={{flexDirection:'row', marginRight:20}}>
                <View style={{width:50, justifyContent:'center'}}>
                <TouchableHighlight onPress={() =>{ Actions.adicionarContato(); props.removeViewAdicionarContato() }} underlayColor="#115354">

                    <Image source={require('../img/adicionar-contato.png')}/>
                
                </TouchableHighlight>
                </View>
                <View style={{justifyContent:'center'}}>
                <TouchableHighlight onPress={() => {props.efetuarLogout() } }>
                    <Text style={{fontSize:20,  color:'#fff'}}>Sair</Text>
                </TouchableHighlight>
                </View>
            </View>
            </View>
        <TabBar {...props} style={{backgroundColor:'#115354', elevation:0}} />
    
    </View>
    
   
);


export default connect(null,{ removeViewAdicionarContato,efetuarLogout })(TabBarMenu);
