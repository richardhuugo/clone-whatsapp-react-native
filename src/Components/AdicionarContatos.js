import React from 'react';
import {View, TextInput, Button,Text} from 'react-native';
import {connect} from 'react-redux';
import { adicionaContato, removeViewAdicionarContato, modificaAdicionaContatoEmail } from '../Actions/AppActions';


class AdicionarContatos extends React.Component{
    _renderAdicionarContato(){
        if(!this.props.cadastro_resultado_inclusao){
            return (
            <View style={{flex:1}} >
                <View style={{flex:1 , justifyContent:'center'}} >
                <TextInput placeholder="E-mail" 
                onChangeText={emaill => { this.props.modificaAdicionaContatoEmail(emaill) } } 
                value={this.props.adiciona_contato_email} style={{fontSize:20, height:45}} />
            </View>
            <View style={{flex:1}} >
               <Button title="Adicionar" color="#115e54" onPress={ () => this.props.adicionaContato(this.props.adiciona_contato_email)}/>
                <Text>{this.props.mensagemContato}</Text>
            </View>
            </View>
            );
        } else {
            return(
                <View>
                    <Text style={{fontSize:20}}>Cadastro Realizado com sucesso.</Text>
                    <Button title="Novo Contato" onPress={() => this.props.removeViewAdicionarContato() }></Button>
                </View>
            );
        }
    }

    render(){
     return   (
            <View style={{flex:1, justifyContent:'center', padding:20}} >
               {this._renderAdicionarContato()}
            </View>
        );
    }
} 

const mapStateToProps = state => (
    {
        adiciona_contato_email: state.AppReducer.adiciona_contato_email,
        mensagemContato: state.AppReducer.mensagemContato,
        cadastro_resultado_inclusao:state.AppReducer.cadastro_resultado_inclusao
    }
);

export default connect(mapStateToProps,{modificaAdicionaContatoEmail,removeViewAdicionarContato, adicionaContato})(AdicionarContatos);