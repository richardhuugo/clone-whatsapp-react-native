import React,{Component} from 'react';
import {View, Text,StyleSheet,ListView,TouchableHighlight} from 'react-native';
import {connect} from 'react-redux';
import {contatosUsuarioFetch} from '../Actions/AppActions/';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';

 class Contatos extends Component{
  
  constructor(props){
    super(props);
    console.ignoredYellowBox = [
      'Setting a timer'
      ];
  }
  
  componentWillMount(){
    this.props.contatosUsuarioFetch();
    this.criaFontDeDados(this.props.contatos);
  }
  componentWillReceiveProps(nextProps){
    this.criaFontDeDados(nextProps.contatos);
  }
  criaFontDeDados( contatos){
    const ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 != r2});
    this.fontDeDados = ds.cloneWithRows(contatos);
  }
  renderRow(contato){
    return(
      <TouchableHighlight
        onPress={() => Actions.Chat({title:contato.nome, contatoNome: contato.nome, contatoEmail: contato.email})}
      >
      <View style={{flex:1, borderBottomWidth:1, borderColor:'#CCC', padding:20}}>      
        <Text>{contato.nome}</Text>
        <Text>{contato.email}</Text>
      </View>
      </TouchableHighlight>
    );
  }
  render(){
    return(

     <ListView 
     enableEmptySections
      dataSource={this.fontDeDados}
      renderRow={this.renderRow  }/>           
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });
  mapStateToProps = state => {
    const contatos = _.map(state.ListarContatosReducer, (val, uid) =>{
      return { ...val, uid}
    })
    return { contatos}
  }
  export default connect(mapStateToProps,{contatosUsuarioFetch})(Contatos);


