import React,{Component} from 'react';
import { View, Text, TextInput, Button,ImageBackground ,ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import * as Progress from 'react-native-progress';

import {modificarEmail,modificarSenha,modificarNome,cadastrarUsuario} from '../Actions/AutenticacaoActions';

class  formCadastro extends React.Component{
    
    _cadastrarUsuario(){

        const { nome, email, senha } = this.props;

        this.props.cadastrarUsuario({nome, email, senha});

    }
    _acessarBTNCadastro(){
        if(this.props.loading_cadastro){
            return(<ActivityIndicator size="large"/>);
        }
        return(<Button 
            title="Cadastrar" 
            color="#115E54" 
            onPress={() => this._cadastrarUsuario()} 
        />);
    }
    render(){
        return (
            <ImageBackground style={{flex:1}} source={require('../img/bg.png')}>
            <View style={{ flex: 1, padding: 10 }}>
                <View style={{ flex: 4, justifyContent: 'center' }}>
               
                    <TextInput 
                        value={this.props.nome} 
                        placeholder="Nome" 
                        style={{ fontSize: 20, height: 45 }}  
                        placeholderTextColor='#fff'  
                        onChangeText={ nome => { this.props.modificarNome(nome)}}
                    />
                    <TextInput 
                        value={this.props.email} 
                        placeholder="E-mail" 
                        style={{ fontSize: 20, height: 45 }}  
                        placeholderTextColor='#fff'
                        onChangeText={ email => { this.props.modificarEmail(email)}} 
                    />
                    <TextInput
                        secureTextEntry 
                        value={this.props.senha} 
                        placeholder="Senha" 
                        style={{ fontSize: 20, height: 45 }}   
                        placeholderTextColor='#fff' 
                        onChangeText={ senha => { this.props.modificarSenha(senha)}}
                    />
                <Text style={{color:'#fff', fontSize:18}}>{this.props.erroCadastro}</Text>
                </View>

                <View style={{ flex: 1 }}>
                    
                    {this._acessarBTNCadastro()}
                </View>
            </View>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state =>({
    nome: state.AuthReducer.nome,
    email:state.AuthReducer.email,
    senha:state.AuthReducer.senha,
    erroCadastro: state.AuthReducer.erroCadastro,
    loading_cadastro: state.AuthReducer.loading_cadastro
  
    
});

export default connect(mapStateToProps, {modificarEmail, modificarSenha, modificarNome, cadastrarUsuario} )(formCadastro);