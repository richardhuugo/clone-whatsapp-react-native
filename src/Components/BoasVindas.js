import React from 'react';
import {View, ImageBackground,Image, Text, Button}  from 'react-native';
import {Actions} from 'react-native-router-flux';
export default BoasVindas =>{
    return (
        <ImageBackground style={{flex:1}} source={require('../img/bg.png')}>
        <View style={{flex:1, padding:15}}>
            <View style={{flex:2, justifyContent:'center', alignItems:'center'}}>
                <Text>Bem Vindo!</Text>
                <Image source={require('../img/logo.png')} />
            </View>
            <View style={{flex:1}}>
                <Button onPress={press => {Actions.formLogin();}} title="Fazer Login" />
            </View>
            
        
        </View>
        </ImageBackground>
    );
}