import React from 'react';
import {View, Text,TextInput,TouchableHighlight,Image, ListView} from 'react-native';
import {connect} from 'react-redux';
import {modificaMensagem,enviarMensagem,conversaUsuarioFecth} from '../Actions/AppActions';
import _ from 'lodash';
 class Chat extends React.Component{

    componentWillMount(){
        this.props.conversaUsuarioFecth(this.props.contatoEmail);
        this._criaFontDeDados(this.props.conversa);

    }
    componentWillReceiveProps(nextProps){
        if(this.props.contatoEmail  != nextProps.contatoEmail){
            this.props.conversaUsuarioFecth(nextProps.contatoEmail);
        }
        this._criaFontDeDados(nextProps.conversa);
    }
    renderRow(texto){
        if(texto.tipo == "e"){
            return(
                <View style={{alignItems: 'flex-end', marginTop:5, marginBottom:5, marginLeft:40}}>
                    <Text style={{borderRadius:10 , fontSize:18, color:'#000', padding:10, backgroundColor:'#dbf5b4', elevation:1}}>{texto.mensagem}</Text>                  
                </View>
            )
        }
        if(texto.tipo == "r"){
            return(
            <View style={{alignItems: 'flex-start', marginTop:5, marginBottom:5, marginRight:40}}>
               <Text style={{borderRadius:10 ,  fontSize:18, color:'#000', padding:10, backgroundColor:'#dbf5b4', elevation:1}}>{texto.mensagem}</Text>                  
            </View>
            )
        }
        
     
    }
     _enviarMensagem(){
         const {mensagem,contatoNome,contatoEmail} = this.props;
         if(mensagem == ""){

         }else{
            this.props.enviarMensagem(mensagem,contatoNome,contatoEmail);
         }
        
     }
     _criaFontDeDados( conversa){
        const ds = new ListView.DataSource({ rowHasChanged: (r1,r2) => r1 != r2});
        this.dataSource = ds.cloneWithRows(conversa);
     }
    render(){
        return(
            <View style={{flex:1, padding:10, backgroundColor:'#eee4dc'}}>
                <View style={{flex:1, paddingBottom:20}}>
                <ListView 
                    enableEmptySections
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                
                />
                </View>
                <View style={{height:60, flexDirection:'row', justifyContent:'center'}}>
                    <TextInput
                        value={this.props.mensagem}
                        onChangeText={ texto => {this.props.modificaMensagem(texto) }}
                     style={{flex:4, borderRadius:10, backgroundColor:'#fff', fontSize:18}}/>
                    <TouchableHighlight onPress={ this._enviarMensagem.bind(this) } underlayColor="#fff" >
                        <Image source={require('../img/enviar_mensagem.png')}/>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}
mapStateToProps = state =>{
    const conversa = _.map(state.ListarConversaReducer, (val,uid) => {
        return {...val, uid};
    });

    return({ mensagem:state.AppReducer.mensagem, conversa});
}
export default connect(mapStateToProps, {modificaMensagem,enviarMensagem,conversaUsuarioFecth})(Chat);