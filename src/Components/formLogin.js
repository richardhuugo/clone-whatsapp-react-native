import React from 'react';
import { View, Text, TextInput, Button, TouchableHighlight,ImageBackground, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect } from 'react-redux';
import {modificarEmailLogin, modificarSenhaLogin,loginUsuario} from '../Actions/AutenticacaoActions';


class formLogin extends React.Component{
    _loginUsuarioFinal(){
        const {  emailLogin, senhaLogin } = this.props;

        this.props.loginUsuario({ emailLogin, senhaLogin});
    }
    _renderBTNAcessar(){
        if(this.props.loading_login){
            return( <ActivityIndicator size="large"/>);
        }
        return (
            <Button title="Acessar" color='#115E54' onPress={() => this._loginUsuarioFinal()} />
        );
    }
    render(){
        return (
            <ImageBackground style={{flex:1}} source={require('../img/bg.png')}>
                    <View style={{ flex: 1, padding: 10 }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 25, color:'#fff' }}>Message</Text>
                        </View>
                        <View style={{ flex: 2}}>
                            <TextInput  
                            value={this.props.emailLogin} 
                            style={{ fontSize: 20, height: 45 }}  
                            placeholder='E-mail' 
                             placeholderTextColor="#fff" 
                             onChangeText={texto => { this.props.modificarEmailLogin(texto) } } />
    
                            <TextInput 
                            secureTextEntry 
                            value={this.props.senhaLogin} 
                            style={{ fontSize: 20, height: 45 }} 
                            placeholder='Senha' 
                            placeholderTextColor='#fff' 
                             onChangeText={senha =>  { this.props.modificarSenhaLogin(senha) } } />
                            <TouchableHighlight onPress={() => {Actions.formCadastro()} }>
                                <Text style={{ fontSize: 20, color:'#fff' }}>Ainda não tem cadastro? Cadastre-se</Text>
                            </TouchableHighlight>
                            
                        </View>
                        <View style={{  alignItems:'center', justifyContent:'center'}}>
                        <Text style={{ fontSize: 20, color:'#ff0000'}} >{this.props.erroLogin}</Text>
                        </View>
                        <View style={{ flex: 2}}>
                        
                           {this._renderBTNAcessar()}
                        </View>
                    </View>
            </ImageBackground>
        );
    }
} 
   


const mapStateToProps = state =>({
    emailLogin: state.AuthReducer.emailLogin,
    senhaLogin: state.AuthReducer.senhaLogin,
    erroLogin: state.AuthReducer.erroLogin,
    loading_login: state.AuthReducer.loading_login
});

export default connect(mapStateToProps,{ modificarSenhaLogin,modificarEmailLogin,loginUsuario })(formLogin);